# Event-level DV Anomaly detection in ATLAS

This repository contains the code for the anomaly detection project for displaced vertices (DVs) in ATLAS. The goal of this project is to develop a machine learning model that can identify anomalous DVs in ATLAS data. This boils down to a binary classification problem: given an event, the model must predict whether the event contains an anomalous DV or not.

## Structure

Dataset generation is done in the [`sw`](sw) directory. Go there for more information on how to generate samples and download already-generated samples.

The [notebooks](notebooks) directory contains Jupyter notebooks that demonstrate how to use the dataset and models. The notebooks are organized by the type of model used.

The models and training code are in the [ad](ad) python module. Currently there are two available models: `DNNClassifier` and `DVT` (Displaced Vertex Transformer). The models are implemented in PyTorch and the training code is implemented in PyTorch Lightning. To use the models, you will first need to install the `ad` module. You can do this by running `pip install -e .` in the root directory of this repository. This command will install the `ad` module in editable mode, meaning that you can modify the code and the changes will be reflected in the installed module.

___

## Training and evaluating models

These models can be trained in two ways: 

#### **(Not recommended) In a notebook:**

See [`notebooks/1_classifier.ipynb`](notebooks/1_classifier.ipynb).

#### **(Recommended) Using configs and the command line:**

The DNN and transformer models can be trained using the `train.py` script in the [cli](cli) directory. This is highly recommended, as Pytorch lightning will be automatically configured to do checkpointing, log metrics to [Weights and Biases](https://wandb.ai/), etc. 

You will need to provide a configuration file containing the hyperparameters for the model. Example configuration files are provided in the [configs](configs) directory. For example to train a DNN model on a predefined dataset, run the following command:

```bash
python -m ad.cli.train configs/dnn.yaml
```

Note that you will need a GPU available on the system to run this script. To run the above script on a SLURM cluster, run the following command:

```bash
sbatch ad/cli/slurm_submit_job.sh configs/dnn.yaml
```

Make sure you update the SLURM script to reflect your cluster's configuration.

**(Optional) Sweeping over hyperparameters**

(SLURM required)

Sometimes you may want to sweep over hyperparameters to find the best combination. This can be done using the `sweep.py` script in the [cli](cli) directory. This script will create several SLURM jobs for each hyperparameter configuration. You will need to provide a configuration file containing the hyperparameters to sweep over. To sweep over a single parameter, you need to just replace the value of the parameter in the configuration file with a list of values. So to sweep over the learning rate, you would modify the `dnn.yaml` file to look like this:

```yaml
model:
  learning_rate: [0.001, 0.01, 0.1]
```

Then you can run the following command to sweep over the learning rate:

```bash
python -m ad.cli.sweep configs/sweeps/dnn.yaml
```

Tada! If you set up wandb correctly, you can easily visualize the results of the sweep in the wandb dashboard.

> Side note: even if you don't want to sweep over hyperparameters, you can still use sweep to automatically submit the SLURM job for you instead of running sbatch yourself. This is what I do.

**(Optional) Weights and Biases**

We use Weights and Biases to log metrics and store model checkpoints. To use Weights and Biases, you will need to create an account and install the `wandb` python package. You will also need to authenticate your account by running `wandb login`. Create a new project on the wandb website and put it's name in your configuration yaml that you will use to train the model.