from setuptools import setup, find_packages

pkgs = find_packages()

setup(
    name="ad",
    author="Sam Young, Daniel Murnane, Julia Gonski",
    version="0.1",
    packages=find_packages(),
    install_requires=[
        "click",
        "pytorch-lightning",
        "wandb",
        "torch",
        "scikit-learn",
        "numpy",
        "tqdm",
        "scipy",
        "dask",
        "uproot",
        "awkward"
    ],
)