import os
import shutil
import re
import yaml
import click
from itertools import product
import subprocess

@click.command()
@click.argument('config_file', type=str, required=True)

def main(config_file):
    with open(config_file) as f:
        config = yaml.load(f, Loader=yaml.FullLoader)

    # Get the path to the directory containing the config
    config_path = os.path.dirname(config_file)

    sweep(config, config_path, config_file)

def sweep(config, config_path, config_file):
    """
    A simple grid scan config for manual sweep
    """

    # Run combo_config on the config file
    exclude = ['data_split', 'signal_files', 'background_files', 'variables']
    config_list = combo_config(config, exclude_keys=exclude)
    sweep_path = os.path.join(config_path, "sweeps")
    os.makedirs(sweep_path, exist_ok=True)

    for run_config in config_list:
        # Save the run_config as a yaml file in the config_path / sweep using shutil
        run_config_path = os.path.join(sweep_path, os.path.basename(config_file))
        if os.path.exists(run_config_path):
            i = 1
            while os.path.exists(run_config_path):
                run_config_path = os.path.join(sweep_path, os.path.basename(config_file).split('.')[0] + str(i) + '.yaml')
                i += 1

        with open(run_config_path, 'w') as f:
            yaml.dump(run_config, f)

        # Then submit the job using the submit function
        submit(run_config_path)

def submit(config_path):
    """
    Submit a batch job with shutil
    """
    # Get the directory containing the current script
    script_dir = os.path.dirname(os.path.realpath(__file__))

    # Construct the absolute path to slurm_submit_job.sh
    slurm_script_path = os.path.join(script_dir, 'slurm_submit_job.sh')

    subprocess.run(['sbatch', slurm_script_path, config_path])

def combo_config(config, exclude_keys=[]):
    """
    Return a list of dictionaries with all possible combinations of values for keys in config
    
    i.e. if config = {
        'a': 1,
        'b': [2, 3],
        'c': [4, 5]
    }

    then combo_config(config) will return:
    
    [
        {'a': 1, 'b': 2, 'c': 4},
        {'a': 1, 'b': 3, 'c': 4},
        {'a': 1, 'b': 2, 'c': 5},
        {'a': 1, 'b': 3, 'c': 5},
    ]
    
    You can also exclude certain keys from being swept by passing them as a list to exclude_keys.
    """
    
    # Extract signal and background file attributes
    GeVs = []
    signal_name = ''
    for file in config['signal_files']:
        basename = file.split('/')[-1]
        GeV = re.findall(r'(\d+)GeV', basename)
        if GeV:            
            GeVs.append(str(GeV[0]))
            signal_name = basename.replace(GeV[0]+"GeV", 'GEV').replace('_events', '')
        else:
            signal_name = basename.replace('_events', '')
    signal_name = signal_name.replace('GEV', "+".join(GeVs)+"GeV").rstrip('.root').rstrip('.h5')
    background_name = config['background_files'][0].split('/')[-1].replace('_events', '').rstrip('.root').rstrip('.h5')
    model_type = config.get('model', 'DNN')
    
    # Determine which keys are being swept (have list values) and not in exclude_keys
    sweep_keys = {k: v for k, v in config.items() if isinstance(v, list) and k not in exclude_keys}

    # Prepare all values for permutation, ensuring even non-list values are wrapped in a list
    total_list = {k: (v if isinstance(v, list) else [v]) for k, v in config.items() if k not in exclude_keys}
    keys, values = zip(*total_list.items())

    # Build list of config dictionaries
    config_list = []
    for bundle in product(*values):
        temp_dict = dict(zip(keys, bundle))
        # Construct the name based only on the sweep keys and their values in the current permutation
        
        name_parts = [model_type, signal_name, background_name] + [
            f"{k}{v}" for k, v in temp_dict.items() if k in sweep_keys
        ]
        temp_dict['name'] = '-'.join(name_parts)
        # Ensure all original keys are included, even those not being swept or excluded
        for k in exclude_keys:
            if k not in temp_dict:
                temp_dict[k] = config[k]
        config_list.append(temp_dict)

    return config_list



if __name__ == "__main__":
    main()
