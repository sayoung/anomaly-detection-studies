import numpy as np
import matplotlib.pyplot as plt
from lightning import Callback
import torch

try:
    import wandb
except ImportError:
    wandb = None


class PlotInputsCallback(Callback):
    def __init__(self):
        super().__init__()
    
    def on_train_start(self, trainer, pl_module):
        assert wandb is not None, "wandb must be installed to use this callback"

        # Generate a plot
        ds = pl_module.train_dataloader().dataset.dataset
        
        fig, ax = plt.subplots(2,3, figsize=(15,10))
        ax = ax.ravel()
        signal = ds.inputs[ds.labels.ravel()==1]
        signal_mask = ds.masks[ds.labels.ravel()==1]
        background = ds.inputs[ds.labels.ravel()==0]
        background_mask = ds.masks[ds.labels.ravel()==0]

        signal_data = signal[signal_mask[...,0] == 0]
        background_data = background[background_mask[...,0] == 0]

        for i in range(len(ds.dtype.names)):
            c,e = np.histogram(signal_data[:,i], bins=100, density=True)
            cen = (e[1:] + e[:-1])/2
            ax[i].plot(cen, c, label='Signal', drawstyle='steps-mid')
            
            c,e = np.histogram(background_data[:,i], bins=100, density=True)
            cen = (e[1:] + e[:-1])/2
            ax[i].plot(cen, c, label='Background', drawstyle='steps-mid')
            ax[i].set_title(ds.dtype.names[i])
            ax[i].set_yscale('log')
            ax[i].legend(frameon=False)
            
        # remove empty plots
        for i in range(len(ds.dtype.names), len(ax)):
            ax[i].axis('off')

        plt.suptitle("Input parameters")

        wandb.log({'input_params':wandb.Image(fig)})
        plt.close(fig)

class AnalysisCallback(Callback):
    def __init__(self):
        super().__init__()
    
    def on_train_end(self, trainer, pl_module):
        assert wandb is not None, "wandb must be installed to use this callback"

        # Generate a confusion matrix
        ds = pl_module.test_dataloader().dataset.dataset
        y_true = ds.labels.ravel()
        
        batch = (ds.inputs, ds.masks, ds.labels)
        device = pl_module.device
        batch = [torch.as_tensor(b).to(device) for b in batch]
        
        output = pl_module.network(batch)
        y_pred = torch.sigmoid(output).detach().cpu().numpy().ravel()

        cm = wandb.plot.confusion_matrix(
            y_true=y_true, preds=y_pred.ravel()>0.5, class_names=["Background", "Signal"]
        )

        wandb.log({"conf_mat": cm})
        y_probas = np.asarray([1-y_pred, y_pred]).T

        wandb.log({"roc": roc_curve(y_true, y_probas, labels=None, classes_to_plot=None)})
        wandb.log({"sic": sic_curve(y_true, y_probas, labels=None, classes_to_plot=None)})
        
def sic_curve(
    y_true=None,
    y_probas=None,
    labels=None,
    classes_to_plot=None,
    title=None,
    split_table=False,
):
    import wandb
    from wandb import util
    from wandb.plots.utils import test_missing, test_types

    np = util.get_module(
        "numpy",
        required="roc requires the numpy library, install with `pip install numpy`",
    )
    pd = util.get_module(
        "pandas",
        required="roc requires the pandas library, install with `pip install pandas`",
    )
    sklearn_metrics = util.get_module(
        "sklearn.metrics",
        "roc requires the scikit library, install with `pip install scikit-learn`",
    )
    sklearn_utils = util.get_module(
        "sklearn.utils",
        "roc requires the scikit library, install with `pip install scikit-learn`",
    )

    y_true = np.array(y_true)
    y_probas = np.array(y_probas)

    if not test_missing(y_true=y_true, y_probas=y_probas):
        return
    if not test_types(y_true=y_true, y_probas=y_probas):
        return

    classes = np.unique(y_true)
    if classes_to_plot is None:
        classes_to_plot = classes

    fpr = dict()
    tpr = dict()
    yprobs = dict()
    indices_to_plot = np.where(np.isin(classes, classes_to_plot))[0]
    for i in indices_to_plot:
        if labels is not None and (
            isinstance(classes[i], int) or isinstance(classes[0], np.integer)
        ):
            class_label = labels[classes[i]]
        else:
            class_label = classes[i]

        fpr[class_label], tpr[class_label], _ = sklearn_metrics.roc_curve(
            y_true, y_probas[..., i], pos_label=classes[i]
        )
        yprobs[class_label] = y_probas[..., i].ravel()

    df = pd.DataFrame(
        {
            "class": np.hstack([[k] * len(v) for k, v in fpr.items()]),
            "ε_B": np.hstack(list(fpr.values())),
            "ε_S": np.hstack(list(tpr.values())),
            "ε_S/√ε_B": np.hstack(list([tpr[k]/np.sqrt(np.where(fpr[k]==0, np.nan, fpr[k])) for k in fpr.keys()])),
            "yprobs": np.hstack(list(yprobs.values()))
        }
    )
    df = df.round(3)

    if len(df) > wandb.Table.MAX_ROWS:
        wandb.termwarn(
            "wandb uses only %d data points to create the plots." % wandb.Table.MAX_ROWS
        )
        # different sampling could be applied, possibly to ensure endpoints are kept
        df = sklearn_utils.resample(
            df,
            replace=False,
            n_samples=wandb.Table.MAX_ROWS,
            random_state=42,
            stratify=df["class"],
        ).sort_values(["ε_B", "ε_S", "ε_S/√ε_B", "yprobs", "class"])

    table = wandb.Table(dataframe=df)
    title = title or "SIC"
    return wandb.plot.line(
        table,
        "ε_S",
        "ε_S/√ε_B",
        title=title,
    )
    
def roc_curve(
    y_true=None,
    y_probas=None,
    labels=None,
    classes_to_plot=None,
    title=None,
    split_table=False,
):
    import wandb
    from wandb import util
    from wandb.plots.utils import test_missing, test_types

    np = util.get_module(
        "numpy",
        required="roc requires the numpy library, install with `pip install numpy`",
    )
    pd = util.get_module(
        "pandas",
        required="roc requires the pandas library, install with `pip install pandas`",
    )
    sklearn_metrics = util.get_module(
        "sklearn.metrics",
        "roc requires the scikit library, install with `pip install scikit-learn`",
    )
    sklearn_utils = util.get_module(
        "sklearn.utils",
        "roc requires the scikit library, install with `pip install scikit-learn`",
    )

    y_true = np.array(y_true)
    y_probas = np.array(y_probas)

    if not test_missing(y_true=y_true, y_probas=y_probas):
        return
    if not test_types(y_true=y_true, y_probas=y_probas):
        return

    classes = np.unique(y_true)
    if classes_to_plot is None:
        classes_to_plot = classes

    fpr = dict()
    tpr = dict()
    indices_to_plot = np.where(np.isin(classes, classes_to_plot))[0]
    for i in indices_to_plot:
        if labels is not None and (
            isinstance(classes[i], int) or isinstance(classes[0], np.integer)
        ):
            class_label = labels[classes[i]]
        else:
            class_label = classes[i]

        fpr[class_label], tpr[class_label], _ = sklearn_metrics.roc_curve(
            y_true, y_probas[..., i], pos_label=classes[i]
        )

    df = pd.DataFrame(
        {
            "class": np.hstack([[k] * len(v) for k, v in fpr.items()]),
            "ε_B": np.hstack(list(fpr.values())),
            "ε_S": np.hstack(list(tpr.values())),
            "ε_S/√ε_B": np.hstack(list([tpr[k]/np.sqrt(np.where(fpr[k]==0, np.nan, fpr[k])) for k in fpr.keys()])),
        }
    )
    df = df.round(3)

    if len(df) > wandb.Table.MAX_ROWS:
        wandb.termwarn(
            "wandb uses only %d data points to create the plots." % wandb.Table.MAX_ROWS
        )
        # different sampling could be applied, possibly to ensure endpoints are kept
        df = sklearn_utils.resample(
            df,
            replace=False,
            n_samples=wandb.Table.MAX_ROWS,
            random_state=42,
            stratify=df["class"],
        ).sort_values(["ε_B", "ε_S", "ε_S/√ε_B", "class"])

    table = wandb.Table(dataframe=df)
    title = title or "ROC"
    return wandb.plot.line(
        table,
        "ε_B",
        "ε_S",
        title=title,
    )