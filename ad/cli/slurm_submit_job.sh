#!/bin/bash

#SBATCH -A atlas:compef
#SBATCH --partition turing
#SBATCH -t 03:00:00
#SBATCH -n 1
#SBATCH --ntasks-per-node=1
#SBATCH --gpus-per-task=1
#SBATCH --cpus-per-task=12
#SBATCH -o logs/%x-%j.out
#SBATCH -J LLP-train
#SBATCH --gpu-bind=none

# Setup
mkdir -p logs

export SLURM_CPU_BIND="cores"
echo -e "\nStarting sweeps\n"

# Single GPU training
srun -u train.py $@