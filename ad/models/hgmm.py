import numpy as np
from sklearn.mixture import GaussianMixture
from logging import getLogger
import logging
import awkward as ak
from sklearn.preprocessing import StandardScaler
from tqdm import tqdm
# import warnings
# from sklearn.exceptions import ConvergenceWarning
# warnings.simplefilter("ignore", ConvergenceWarning)

logger = getLogger("HGMM")
logger.setLevel("INFO")

class HierarchicalGMM:
    _fitted = False

    def __init__(self, config={}, **kwargs):
        if not config:
            config = kwargs

        self.params = config["params"]
        self.rng = np.random.default_rng(config.get("seed", None))
        if config.get("verbose", True):
            logging.basicConfig(level=logging.INFO)

        self.event_gmm = None  # event level GMM
        self.event_n_components = config.get("event_n_components", 100)
        self.event_reg_covar = config.get("event_reg_covar", 1e-5)
        self.event_init_params = config.get("event_init_params", "kmeans")
        self.global_reg_covar = config.get("global_reg_covar", 1e-5)
        
        self.n_track_gmm = None  # GMM for number of tracks
        self.n_components_per_event = None

        self.soft_pt_cut = config.get("soft_pt_cut", 0.5)
        self.hard_pt_cut = config.get("hard_pt_cut", 10)
        self.eta_cut = config.get("eta_cut", 4.5)
        self.apply_selection = config.get("apply_selection", False)

        self._x_indices = None
        self._y_indices = None
        self._X = None

    def fit(self, X: ak.Array):
        assert isinstance(X, ak.Array), "X must be an awkward array"

        # X = self._transform_parameters(X)
        # Fit means, covariances, and weights for each component
        means = []
        covariances = []
        weights = []
        n_components = []
        logger.info("Fitting event-level GMMs")
        for event in tqdm(X):
            data = np.array([event[param].to_list() for param in self.params]).T
            gmm = find_optimal_gmm(data, min_components=3, max_components=7, reg_covar=self.event_reg_covar)
            means.append(gmm.means_)
            covariances.append(gmm.covariances_)
            weights.append(gmm.weights_)
            n_components.append(gmm.n_components)
        means = np.concatenate(means)
        covariances = np.concatenate(covariances)
        weights = np.concatenate(weights)
        vals, counts = np.unique(n_components, return_counts=True)
        freqs = counts / counts.sum()
        self.n_components_per_event = (vals, freqs)
        
        cholesky_list = np.linalg.cholesky(covariances)
        # diag_elements = cholesky_list[:, np.eye(len(self.params), dtype=bool)]
        # diag_elements[:] = np.log(diag_elements)

        cholesky_features = np.array(
            [cov[np.tril_indices_from(cov)] for cov in cholesky_list]
        )

        # construct total data
        total_data = np.hstack(
            [means, cholesky_features, weights[:, np.newaxis]]
        )
        self._total_data = total_data
        # scale total data
        self.scaler = StandardScaler()
        total_data_scaled = self.scaler.fit_transform(total_data)
    
        # 2. Fit event GMM        
        self.event_gmm = GaussianMixture(
            self.event_n_components,
            covariance_type="full",
            random_state=self.rng.integers(0, 2**32),
            reg_covar=self.global_reg_covar,
            init_params=self.event_init_params,
        ).fit(total_data_scaled)

        # 3. Fit num track GMM
        num_tracks = np.log10(
            ak.num(X[self.params[0]], axis=1).to_numpy().reshape(-1, 1)
        )
        self.n_track_gmm = find_optimal_gmm(num_tracks, 4)
        logger.info("Fit n_track_per_event estimator")

        self._X = X
        self._fitted = True
        
    def refit_event_gmm(self, n_components=100, reg_covar=1e-5):
        assert self._fitted, "Model not fitted yet"
        self.event_n_components = n_components
        self.event_reg_covar = reg_covar
        total_data_scaled = self.scaler.transform(self._total_data)
        self.event_gmm = find_optimal_gmm(
            total_data_scaled,
            max_components=n_components,
            min_components=15,
            reg_covar=reg_covar,
        )

    def _sample_n_track(self, n_samples=1):
        assert self._fitted, "Model not fitted yet"

        logn_track = self.n_track_gmm.sample(n_samples)[0].ravel()
        n_track = np.round(10**logn_track, 0).astype(int)
        n_track = np.clip(n_track, 3, None)
        return n_track

    def sample(self, n_samples=1, clip_covar=5e-1):
        assert self._fitted, "Model not fitted yet"

        n_tracks = self._sample_n_track(n_samples)
        events = []    
        for n_track in n_tracks:
            # sample number of components
            vals, freqs = self.n_components_per_event
            n_components = np.random.choice(vals, p=freqs)
            
            # sample from the event-level GMM, then transform back to original space
            sampled_scaled = self.event_gmm.sample(n_components)[0]
            # sampled_scaled = self.pca.inverse_transform(sampled_pca)        # undo PCA
            inverse_scaled = self.scaler.inverse_transform(sampled_scaled)  # undo scaling

            # split the inverse_scaled into means, cholesky features, and weights
            n_means = len(self.params)
            n_chol_feats = (n_means * (n_means + 1)) // 2 # n(n+1)/2 cholesky features (lower triangular mx)
            means, cholesky_feats, weights = np.split(
                inverse_scaled,
                [n_means, n_means + n_chol_feats],
                axis=1,
            )

            # clip weights to be between 0 and 1
            weights = np.clip(weights, 0, 1).squeeze()
            
            # if all weights are 0, set them to be uniform
            if sum(weights) == 0:
                weights = np.ones(n_components) / n_components
            weights = weights / np.sum(weights)

            # reconstruct covariance matrices from cholesky factors
            reconstructed_cholesky = np.zeros((n_components, n_means, n_means))
            covariances = np.zeros((n_components, n_means, n_means))
            for i, chol in enumerate(cholesky_feats):
                reconstructed_cholesky[i][np.tril_indices(n_means)] = chol
                # ensure diagonal elements are positive
                diag_elements = reconstructed_cholesky[i][np.eye(n_means, dtype=bool)]
                diag_elements[:] = np.clip(diag_elements, 0, None)
                # exponentiate the diagonal element to untransform the cholesky factor
                # reconstructed_cholesky[i][np.eye(n_means, dtype=bool)] = np.exp(
                #     reconstructed_cholesky[i][np.eye(n_means, dtype=bool)]
                # )

                covariances[i] = np.dot(reconstructed_cholesky[i], reconstructed_cholesky[i].T)

            # adjust covariance matrix to not have super small eigenvalues (smooths out the sampling)
            eigenvalues, eigenvectors = np.linalg.eigh(covariances)
            eigenvalues_adjusted = np.clip(eigenvalues, clip_covar, None)
            covariances = np.array(
                [
                    eigenvectors[i] @ np.diag(eigenvalues_adjusted[i]) @ eigenvectors[i].T
                    for i in range(len(covariances))
                ]
            )

            # sample the number tracks belonging to each component
            n_samples_comp = np.random.multinomial(n_track, weights).ravel()

            # sample from each component individually, then concatenate
            samples = []
            for mean, covariance, n_sample in zip(means, covariances, n_samples_comp):
                sample = np.random.multivariate_normal(mean, covariance, n_sample)
                samples.append(sample)
            samples = np.concatenate(samples)
            events.append(samples)
            
        # create Awkward array from the sampled events
        events = ak.Array(
            {p: [event[:,i] for event in events]
            for i, p in enumerate(self.params)}
        )

        # TODO(youngsam): Phi isn't totally random -- it's hard to model tho
        events['Track.Phi'] = [np.random.uniform(-np.pi, np.pi, n_track) for n_track in n_tracks]
        events = self._inv_transform_parameters(events)

        if self.apply_selection:
            events = self.select(events)
        return events

    def _transform_parameters(self, X):
        for param in X.fields:
            if '.PT' in param:
                X[param.replace('PT', 'logPT')] = np.log10(X[param])
                
            elif '.D' in param:
                nonzero = X[param] != 0
                nonzero_min = ak.min(abs(X[param][nonzero]))
                X[param.replace('.D', '.slogD')] = symlog(X[param], nonzero_min)
                X[param.replace('.D', '') + '_min'] = nonzero_min
        return X
    
    def _inv_transform_parameters(self, X):
        for param in X.fields:
            if '.slog' in param:
                min_val = self._X[param.replace('slog', '') + '_min'][0]
                X[param.replace('slog', '')] = inv_symlog(X[param], min_val)
            elif '.logPT' in param:
                X[param.replace('logPT', 'PT')] = 10 ** X[param]
            elif ".PT" in param:
                # clip PT to smallest nonzero value
                X[param] = np.clip(
                    X[param],
                    ak.min(X[param][X[param] > 0]),
                    None,
                )

        return X


def find_optimal_gmm(
    X: np.ndarray, max_components: int, min_components = 1, reg_covar=1e-6
):
    """Find the optimal number of components for a GMM using BIC."""

    assert max_components > 3, "max_components must be greater than 2"
    min_bic = np.inf
    best_gmm = None
    for num_components in range(min_components, max_components + 1):
        if num_components > X.shape[0]:
            if min_bic == np.inf:
                raise ValueError(
                    f"Number of components {num_components} greater than number of samples {X.shape[0]}."
                )
            break
        
        gmm = GaussianMixture(num_components, reg_covar=reg_covar,
                              init_params='random_from_data')
        gmm.fit(X)

        bic = gmm.bic(X)
        if bic < min_bic:
            min_bic = bic
            best_gmm = gmm

    return best_gmm

def symlog(x, coeff=1e-5):
    return np.arcsinh(x / coeff)


def inv_symlog(y, coeff=1e-5):
    return np.sinh(y) * coeff


def logit(x, eps=1e-10):
    x = np.clip(x, eps, 1 - eps)
    return np.log(x / (1 - x))


def inv_logit(x):
    return 1 / (1 + np.exp(-x))
