import numpy as np
import torch
from torch.utils.data import Dataset, Sampler, DataLoader, random_split

import sys
import lightning as pl
import warnings
import torch.nn.functional as F
from sklearn.metrics import roc_auc_score

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

from .utils import get_optimizers, DVDataset, H5DVDataset
from .networks.dnn import DNNClassifier
from .networks.transformer import DVT

class TrackRegression(pl.LightningModule):
    def __init__(self, hparams):
        super().__init__()
        """
        Initialise the Lightning Module that can scan over different GNN training regimes
        """

        self.save_hyperparameters(hparams)

        # Assign hyperparameters
        self.trainset, self.valset, self.testset = None, None, None

        model_cls = DNNClassifier if hparams.get("model", "DNN")=='DNN' else DVT
        self.network = model_cls(hparams)
        
        self.loss_fn = torch.nn.BCEWithLogitsLoss()

    def setup(self, stage="fit"):
        """
        The setup logic of the stage.
        1. Setup the data for training, validation and testing.
        2. Run tests to ensure data is of the right format and loaded correctly.
        3. Construct the truth and weighting labels for the model training
        """
        self.load_data(stage)
        torch.set_float32_matmul_precision("medium" if stage == "fit" else "high")
        
        try:
            print("Defining figures of merit")
            self.logger.experiment.define_metric("val_loss", summary="min")
            self.logger.experiment.define_metric("val_acc", summary="max")
        except Exception:
            warnings.warn(
                "Failed to define figures of merit, due to logger unavailable"
            )

    def load_data(self, stage):
        """
        Load in the data for training, validation and testing.
        """

        dataset = H5DVDataset(
            num=-1,
            signal_files=self.hparams["signal_files"],
            background_files=self.hparams["background_files"],
            num_high_pt=self.hparams["num_high_pt"],
            read_in_memory=self.hparams["read_in_memory"],
        )
                
        self.trainset, self.valset = random_split(
            dataset, self.hparams["data_split"]
        )

    def get_dataloader(self, stage: str, dataset: DVDataset, shuffle: bool):
        drop_last = stage == "fit"
        return DataLoader(
            dataset=dataset,
            shuffle=shuffle,
            batch_size=self.hparams.get("batch_size", 1024),
            num_workers=self.hparams.get("num_workers", 1),
            pin_memory=self.hparams.get("pin_memory", True),
            drop_last=drop_last,
        )

    def train_dataloader(self):
        if self.trainset is None:
            return None
        return self.get_dataloader("fit", self.trainset, shuffle=True)

    def val_dataloader(self):
        if self.valset is None:
            return None
        return self.get_dataloader("val", self.valset, shuffle=False)
    
    def predict_dataloader(self):
        return [self.train_dataloader(), self.val_dataloader()]

    def configure_optimizers(self):
        optimizer, scheduler = get_optimizers(self.parameters(), self.hparams)
        return optimizer, scheduler

    def forward(self, batch):
        return self.network(batch)

    def training_step(self, batch, batch_idx):
        output = self(batch)
        loss, truth = self.loss_function(output, batch)

        self.log(
            "train_loss",
            loss,
            on_step=False,
            on_epoch=True,
            batch_size=1,
            sync_dist=True,
        )

        return loss

    def loss_function(self, output, batch):
        """
        An MSE loss that targets the batch[2] labels, using the hparams[labels]
        """

        *__, labels = batch
        return self.loss_fn(output, labels), labels

    def shared_evaluation(self, batch, batch_idx):
        output = self(batch)
        loss, truth = self.loss_function(output, batch)

        current_lr = self.optimizers().param_groups[0]['lr']

        self.log_dict(
            {
                "learning_rate": current_lr,
                "val_loss": loss,
            },
            on_step=False,
            on_epoch=True,
            sync_dist=True,
        )

        return {
            "loss": loss.detach(),
            "truth": truth,
            "output": output.detach(),
            "batch": batch,
        }

    def validation_step(self, batch, batch_idx):
        out = self.shared_evaluation(batch, batch_idx)
        self.log_metrics(out['output'], out['truth'], out['loss'])

    def test_step(self, batch, batch_idx):
        return self.shared_evaluation(batch, batch_idx)

    def log_metrics(self, output, target_truth, loss):
        preds = torch.sigmoid(output) > 0.5

        # Positives
        edge_positive = preds.sum().float()

        # Signal true & signal tp
        target_true = target_truth.sum().float()
        target_true_positive = (target_truth.bool() & preds).sum().float()

        # add torch.sigmoid(output).float() to convert to float in case training is done with 16-bit precision
        target_auc = roc_auc_score(
            target_truth.bool().cpu().detach(),
            torch.sigmoid(output).float().cpu().detach(),
        )

        # Eff, pur, auc
        accuracy = (preds == target_truth.bool()).sum().float() / len(preds)
        target_eff = target_true_positive / target_true
        target_pur = target_true_positive / edge_positive

        self.log_dict(
            {
                "val_acc": accuracy,
                "target_eff": target_eff,
                "target_pur": target_pur,
                "auc": target_auc,
            },  # type: ignore
            sync_dist=True,
            batch_size=1,
            on_epoch=True,
            on_step=False,
        )

        return preds

    def on_train_start(self):
        self.trainer.strategy.optimizers = [
            self.trainer.lr_scheduler_configs[0].scheduler.optimizer
        ]        

    def on_before_optimizer_step(self, optimizer, *args, **kwargs):
        # warm up lr
        if (self.hparams["warmup"] is not None) and (
            self.trainer.current_epoch < self.hparams["warmup"]
        ):
            lr_scale = min(
                1.0, float(self.trainer.current_epoch + 1) / self.hparams["warmup"]
            )
            for pg in optimizer.param_groups:
                pg["lr"] = lr_scale * self.hparams["lr"]

        # after reaching minimum learning rate, stop LR decay
        for pg in optimizer.param_groups:
            pg["lr"] = max(pg["lr"], self.hparams.get("min_lr", 0))

