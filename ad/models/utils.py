from typing import List, Union

import awkward as ak
import h5py
import numpy as np
import numpy.ma as ma
import torch
import torch.nn as nn
import uproot
from numpy.lib.recfunctions import structured_to_unstructured as s2u
from torch.utils.data import Dataset, Sampler
import pytorch_lightning as pl
import matplotlib.pyplot as plt

def get_optimizers(parameters, hparams):
    optimizer = [
        torch.optim.AdamW(
            parameters,
            lr=(hparams["lr"]),
            betas=(0.9, 0.999),
            eps=1e-08,
            amsgrad=True,
        )
    ]

    if (
        "scheduler" not in hparams
        or hparams["scheduler"] is None
        or hparams["scheduler"] == "StepLR"
    ):
        scheduler = [
            {
                "scheduler": torch.optim.lr_scheduler.StepLR(
                    optimizer[0],
                    step_size=hparams["patience"],
                    gamma=hparams["factor"],
                ),
                "interval": "epoch",
                "frequency": 1,
            }
        ]
    elif hparams["scheduler"] == "ReduceLROnPlateau":
        scheduler = [
            {
                "scheduler": torch.optim.lr_scheduler.ReduceLROnPlateau(
                    optimizer[0],
                    mode="min",
                    factor=hparams["factor"],
                    patience=hparams["patience"],
                    verbose=True,
                ),
                "interval": "epoch",
                "frequency": 1,
            }
        ]
    elif hparams["scheduler"] == "CosineAnnealingWarmRestarts":
        scheduler = [
            {
                "scheduler": torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(
                    optimizer[0],
                    T_0=hparams["patience"],
                    T_mult=2,
                    eta_min=1e-8,
                    last_epoch=-1,
                ),
                "interval": "epoch",
                "frequency": 1,
            }
        ]
    else:
        raise ValueError(f"Unknown scheduler: {hparams['scheduler']}")

    return optimizer, scheduler


class RandomBatchSampler(Sampler):
    def __init__(
        self,
        dataset: torch.utils.data.Dataset,
        batch_size: int,
        shuffle: bool = False,
        drop_last: bool = False,
    ):
        """Batch sampler for an h5 dataset.

        The batch sampler performs weak shuffling. Jets are batched first,
        and then batches are shuffled.

        Parameters
        ----------
        dataset : torch.data.Dataset
            Input dataset
        batch_size : int
            Number of events to batch
        shuffle : bool
            Shuffle the batches
        drop_last : bool
            Drop the last incomplete batch (if present)
        """
        self.batch_size = batch_size
        self.dataset_length = len(dataset)
        self.n_batches = self.dataset_length / self.batch_size
        self.nonzero_last_batch = int(self.n_batches) < self.n_batches
        self.drop_last = drop_last
        self.shuffle = shuffle

    def __len__(self):
        return int(self.n_batches) + int(not self.drop_last and self.nonzero_last_batch)

    def __iter__(self):
        if self.shuffle:
            self.batch_ids = torch.randperm(int(self.n_batches))
        else:
            self.batch_ids = torch.arange(int(self.n_batches))
        # yield full batches from the dataset
        for batch_id in self.batch_ids:
            start, stop = batch_id * self.batch_size, (batch_id + 1) * self.batch_size
            yield np.s_[int(start) : int(stop)]

        # in case the batch size is not a perfect multiple of the number of samples,
        # yield the remaining samples
        if not self.drop_last and self.nonzero_last_batch:
            start, stop = int(self.n_batches) * self.batch_size, self.dataset_length
            yield np.s_[int(start) : int(stop)]


class PFlowDataset(Dataset):
    def __init__(
        self,
        filename: str,
        num: int = -1,
        variables: dict = None,
        labels: dict = None,
    ):
        """A map-style dataset for loading jets from a structured array file.

        Parameters
        ----------
        filename : str
            Input h5 filepath containing structured arrays
        num : int
            Number of events to load, -1 for all events in the file
        variables: dict
            A dictionary containing lists of variables
        labels: dict
            A dictionary containing lists of labels
        """
        super().__init__()

        # check labels have been configured
        self.filename = filename
        self.file = h5py.File(self.filename, "r")

        # set the variables to load
        self.variables = variables

        # set the labels to load
        self.labels = labels

        # if variables none, then set it to be a dictionary, with each key a list of the keys in that hdf5 dataset
        if self.variables is None:
            self.variables = {
                key: list(self.file[key].keys()) for key in self.file.keys()
            }

        # set the number of events to load
        self.num = self.get_num(num)

    def __len__(self):
        return int(self.num)

    def __getitem__(self, jet_idx):
        """Return on sample or batch from the dataset.

        Parameters
        ----------
        jet_idx
            A numpy slice corresponding to a batch of jets.

        Returns
        -------
        tuple
            Dict of tensor for each of the inputs, masks, and labels.
            Each tensor will contain a batch of samples.
        """
        inputs = {}
        labels = {}
        masks = {}

        for input_type, input_features in self.variables.items():
            # Create a NumPy array to hold the data
            batch_size = jet_idx.stop - jet_idx.start
            batch = np.empty(
                (batch_size,) + self.file[input_type].shape[1:],
                dtype=self.file[input_type].dtype,
            )

            # Read data from 'self.file[input_type]' directly into 'batch'
            self.file[input_type].read_direct(
                batch, np.s_[jet_idx.start : jet_idx.stop]
            )

            flat_array = s2u(batch[input_features], dtype=np.float32).copy()
            inputs[input_type] = torch.from_numpy(flat_array)

            # apply the input padding mask
            if "valid" in batch.dtype.names:
                masks[input_type] = ~torch.from_numpy(batch["valid"])
                if input_type not in ["scalar"]:
                    inputs[input_type][masks[input_type]] = 0

            # check inputs are finite
            if not torch.isfinite(inputs[input_type]).all():
                raise ValueError(
                    f"Non-finite inputs for '{input_type}' in {self.filename}."
                )

            # process labels for this input type
            if input_type in self.labels:
                labels[input_type] = {}
                for label in self.labels[input_type]:
                    dtype = (
                        torch.long
                        if np.issubdtype(batch[label].dtype, np.integer)
                        else None
                    )
                    labels[input_type][label] = torch.as_tensor(
                        batch[label].copy(), dtype=dtype
                    )

        return inputs, masks, labels

    def get_num(self, num_requested: int):
        num_available = len(self.file["scalar"])

        # not enough jets
        if num_requested > num_available:
            raise ValueError(
                f"Requested {num_requested:,} jets, but only {num_available:,} are"
                f" available in the file {self.filename}."
            )

        # use all jets
        if num_requested < 0:
            return num_available

        # use requested jets
        return num_requested


class DVDataset(Dataset):
    def __init__(
        self,
        signal_files: Union[str, List[str]],
        background_files: Union[str, List[str]],
        variables: dict = None,
        num: int = -1,
        num_high_pt: int = 80,
        soft_pt_cut: float = None,  # GeV
        read_in_memory: bool = False,
    ):
        """A map-style dataset for loading jets containing displaced vertices
        from one or more ROOT file(s).

        This dataset is to be used to train a classifier to distinguish between sets of tracks
        stemming from LLP decays and those stemming from QCD jets in a CWoLA-like fashion.

        Parameters
        ----------
        signal_files : str
            Input ROOT filepath(s) of signal-containing ROOT files containing relevant branches
        background_files : str
            Input ROOT filepath(s) of background-containing ROOT files containing relevant branches
        num : int
            Number of events to load, -1 for all events in the file
        variables: dict
            A dictionary containing keys of branch names and values of a list of variables to use
            for training (from each ROOT file).
        num_high_pt: int
            Number of high pt tracks to keep (default 80)
        read_in_memory: bool
            Whether to read the entire dataset into memory instead of doing it on the fly
            during training (default False)
        """
        super().__init__()

        if isinstance(signal_files, str):
            signal_files = [signal_files]
        if isinstance(background_files, str):
            background_files = [background_files]

        self.num_high_pt = num_high_pt
        self.soft_pt_cut = soft_pt_cut

        self.variables = variables

        # if variables none, then set it to be a dictionary, with each key a list of the keys in that ROOT dataset
        if self.variables is None:
            self.variables = [
                "Track.PT",
                "Track.Eta",
                "Track.Phi",
                "Track.DZ",
                "Track.D0",
            ]
        assert (
            "Track.PT" in self.variables
        ), "Track.PT must be included in the variables"

        self.data = self.read_data(signal_files, background_files)
        self.num = self.get_num(num)

        if read_in_memory:
            self.inputs, self.masks, self.labels = self.slice_dataset(
                slice(0, self.num)
            )
            del self.data

    def __len__(self):
        return self.num

    def read_data(self, signal_files: List[str], background_files: List[str]):
        # read in data
        s_data = uproot.dask(signal_files, library="ak")
        b_data = uproot.dask(background_files, library="ak")

        def get_mask(data):
            mask = ak.num(data['Track.PT'], axis=1) > 0
            
            if self.soft_pt_cut:
                mask = mask & (data['Track.PT'] > self.soft_pt_cut)
            return mask

        mask_s = get_mask(s_data)
        mask_b = get_mask(b_data)

        p = []
        min_len, min_file = np.inf, None
        for i, (file, fnames) in enumerate(
            zip([b_data, s_data], [background_files, signal_files])
        ):
            # label the files (1 for signal, 0 for background)
            file["label"] = i * ak.ones_like(file["Track.PT"])

            # calculate the probability of an event from each file being selected
            lens = []
            for f in fnames:
                data = uproot.dask(f, library='ak')
                mask = get_mask(data)
                n_nonempty = ak.sum(ak.num(data['Track.PT'][mask], axis=1) > 0) 
                lens.append(n_nonempty.compute())

            p.append(np.concatenate([[1 / a] * a for a in lens]) / len(fnames))

            # find the minimum number of events in all subfiles; we will not be able to sample more than 2x this number
            if sum(lens) < min_len:
                min_len = sum(lens)

        self.min_len = min_len

        # permute data, weighting higher choice probability to files with fewer events
        p_b, p_s = p
        assert np.isclose(p_s.sum(), 1) and np.isclose(p_b.sum(), 1), "probabilities must sum to 1"
        permute = lambda x, p: x[np.random.choice(len(p), size=min_len, p=p, replace=False)]
        s_data = permute(s_data[self.variables+['label']][mask_s].compute(), p_s)
        b_data = permute(b_data[self.variables+['label']][mask_b].compute(), p_b)

        # combine the permuted files and shuffle the events
        data = ak.concatenate([b_data, s_data])
        data = data[np.random.permutation(2*min_len)]
        return data

    def slice_dataset(self, track_idx: slice):
        """
        Slice the Awkward array dataset into a batch of Tensors using the given slice.
        """

        n_tracks = self.num_high_pt

        # read in batch and sort it by PT, keeping n_tracks with highest PT
        batch = self.data[self.variables + ["label"]][track_idx]
        
        # normalize PT to sum to 1
        batch["Track.PT"] = batch["Track.PT"] / ak.sum(batch["Track.PT"], axis=1)

        sorted_batch = batch[ak.argsort(batch["Track.PT"], axis=1)[:, -n_tracks:]]

        # pad the batch with zeros up to n_tracks
        padded_batch = ak.pad_none(sorted_batch, n_tracks, clip=True)

        # stack the padded batch into an unstructured numpy array
        arr = ma.stack(
            [padded_batch[feat, ...].to_numpy() for feat in self.variables], axis=2
        )

        # convert to torch tensors
        inputs = torch.from_numpy(arr.filled(fill_value=0))  # (N, C, T)
        masks = torch.from_numpy(arr.mask).bool()  # (N, C, T)

        # check inputs are finite
        if not torch.isfinite(inputs).all():
            raise ValueError(f"Non-finite inputs for '{self.variables}'.")

        # include labels
        batch_labels = padded_batch["label"][track_idx]
        labels = torch.from_numpy(batch_labels.to_numpy().filled(0))[..., [0]]  # (N, 1)

        return inputs, masks, labels

    def __getitem__(self, track_idx: slice):
        """Return on sample or batch from the dataset.

        Parameters
        ----------
        traj_idx
            A numpy slice corresponding to a batch of trajectories.

        Returns
        -------
        tuple
            Dict of tensor for each of the inputs, masks, and labels.
            Each tensor will contain a batch of samples.
        """

        if hasattr(self, "inputs"):
            inputs = self.inputs[track_idx]
            masks = self.masks[track_idx]
            labels = self.labels[track_idx]
            return inputs, masks, labels

        return self.slice_dataset(track_idx)

    def get_num(self, num_requested: int):
        assert int(num_requested) == num_requested, "num_requested must be an integer"

        # not enough jets
        if num_requested > 2 * self.min_len:
            raise ValueError(
                f"Requested {num_requested:,} jets, but only {self.min_len:,} are"
                f" available; more events can't"
                f" be sampled without inducing bias in the dataset."
            )

        # use all jets
        if num_requested < 0:
            return 2 * self.min_len

        # use requested jets
        return num_requested
    
class H5DVDataset(Dataset):
    def __init__(
        self,
        signal_files: Union[str, List[str]],
        background_files: Union[str, List[str]],
        num_high_pt=80,
        num=-1,
        read_in_memory=False,
    ):

        super().__init__()

        if isinstance(signal_files, str):
            signal_files = [signal_files]
        if isinstance(background_files, str):
            background_files = [background_files]

        self.num_high_pt = num_high_pt

        self.data = self.read_data(signal_files, background_files)
        _, self.idx, _ = self.data
        self.num = self.get_num(num)
        
        if read_in_memory:
            inputs, masks, labels = list(zip(*iter(self)))
            self.inputs = np.asarray(inputs)
            self.masks = np.asarray(masks)
            self.labels = np.asarray(labels)
            del self.data

    def __len__(self):
        return self.num

    def read_data(self, signal_files: List[str], background_files: List[str]):
        assert all([f.endswith('.h5') for f in signal_files + background_files]), "all files must be .h5 files"
        
        # find minimum length of all events for each file
        min_lengths = dict(signal=[], background=[])
        for files, dtype in ((signal_files, 'signal'), (background_files, 'background')):
            for file in files:
                with h5py.File(file, 'r') as f:
                    lengths = f['lengths'][:]
                    min_lengths[dtype].append((lengths>0).sum())
        
        # calc the maximum number of events we can take from each file type to maintain balance
        tot_events_lim = min(sum(min_lengths['signal']), sum(min_lengths['background']))
        self.min_len = tot_events_lim

        signal_events = tot_events_lim // len(signal_files)
        background_events = tot_events_lim // len(background_files)
        
        data = dict()
        for files, dtype, min_len in ((signal_files, 'signal', signal_events),
                                      (background_files, 'background', background_events)):
            d = []
            l = []
            for file in files:
                with h5py.File(file, 'r') as f:
                    lengths = f['lengths'][:]
                    lengths = lengths[lengths > 0]
                    lengths = lengths[:min_len]

                    d.append(f['Track'][:lengths.sum()])
                    l.append(lengths)
                    
            data[dtype+'_data'] = np.concatenate(d, axis=0)
            data[dtype+'_lens'] = np.concatenate(l, axis=0)


        lens = np.concatenate([data['signal_lens'], data['background_lens']], axis=0)
        idx = np.concatenate([[0], np.cumsum(lens)], axis=0)
        labels = np.concatenate([np.ones(data['signal_lens'].shape[0]), np.zeros(data['background_lens'].shape[0])], axis=0)
        self.dtype = data['signal_data'].dtype
        data = s2u(np.concatenate([data['signal_data'], data['background_data']], axis=0))
        return data, idx, np.expand_dims(labels, axis=1)

    def __getitem__(self, track_idx: int):
        if hasattr(self,'inputs'):
            inputs, masks, labels = self.inputs, self.masks, self.labels
            return inputs[track_idx], masks[track_idx], labels[track_idx]
        
        inputs, idx, labels = self.data
        start, end = idx[track_idx], idx[track_idx+1]
        data = inputs[start:end][:self.num_high_pt]
        label = labels[track_idx]

        data = np.pad(data, ((self.num_high_pt-data.shape[0], 0), (0, 0)), constant_values=0)
        mask = np.pad(np.zeros_like(data), ((self.num_high_pt-data.shape[0], 0), (0, 0)), constant_values=1).astype(bool)
        return data, mask, label

    def get_num(self, num_requested: int):
        assert int(num_requested) == num_requested, "num_requested must be an integer"

        # not enough jets
        if num_requested > self.idx.shape[0]-1:
            raise ValueError(
                f"Requested {num_requested:,} jets, but only {self.idx.shape[0]-1:,} are"
                f" available; more events can't"
                f" be sampled without inducing bias in the dataset."
            )

        # use all jets
        if num_requested < 0:
            return self.idx.shape[0]-1

        # use requested jets
        return num_requested