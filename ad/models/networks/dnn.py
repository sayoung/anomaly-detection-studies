import torch.nn as nn
import torch
from .utils import make_mlp

class DNNClassifier(nn.Module):
    def __init__(
        self,
        hparams:dict,
    ):
        """Deep neural network classifier

        A simple feed-forward neural network with a configurable number of hidden layers,
        activation function, and output activation function.
        
        Parameters
        ----------
        hparams : dict
            Dictionary of hyperparameters for the model. Must contain the following keys:
            - input_features: int
                The number of input features
            - hidden_features: int
                The number of features in each hidden layer
            - hidden_layers: int
                The number of total hidden layers to use
            - output_features: int
                The number of output features
            - hidden_activation: str
                Activation function to use for hidden layers. Must be an attribute of `torch.nn`
            - output_activation: str
                Activation function to use for the output layer. Must be an attribute of `torch.nn`
            - layer_norm: bool
                Whether to use layer normalization
            - batch_norm: bool
                Whether to use batch normalization
            - input_dropout: float
                Probability of an element in the input layer to be zeroed out
            - hidden_dropout: float
                Probability of an element in each hidden layer to be zeroed out
            - final_reduction: str
                Reduction to apply to the output. Must be either "mean" or "sum"
        
        """
        super(DNNClassifier, self).__init__()

        self.net = make_mlp(
            hparams['input_features'],
            [hparams['hidden_features']] * hparams['hidden_layers'] + [hparams['output_features']], 
            hidden_activation=hparams['hidden_activation'],
            output_activation=hparams['output_activation'],
            layer_norm=hparams['layer_norm'],
            batch_norm=hparams['batch_norm'],
            input_dropout=hparams['input_dropout'],
            hidden_dropout=hparams['hidden_dropout'],
        )

        if hparams['final_reduction'] == 'mean':
            self.final_reduction = lambda x, mask: x.sum(dim=1) / mask.sum(dim=1)
        else:
            self.final_reduction = lambda x, mask: torch.sum(x, dim=1)

        # initialize weights
        for m in self.modules():
            if isinstance(m, nn.Linear):
                nn.init.xavier_normal_(m.weight, gain=2)
                nn.init.zeros_(m.bias)

    def forward(self, batch):
        # N = batch size, C = num. features, T = num. tracks
        # input: (N, T, C)
        # mask: (N, T, C) -- real track = 1, padded = 0
        x, mask, __ = batch

        mask = ~mask[..., [0]] # (N, T, 1)

        hit_linear = False
        for layer in self.net:
            x = layer(x)

            # multiply by mask to zero out padded tracks
            if not hit_linear and isinstance(layer, nn.Linear):
                x *= mask
                hit_linear = True

        return self.final_reduction(x, mask)
    
