#!/bin/bash
 
###############################################################
# Sam Young (youngsam@stanford.edu)
#
# Converts a bunch of pileup events in hepmc format as a single
# file in pileup format.
###############################################################


#SBATCH --partition=roma
#SBATCH --account=atlas:usatlas
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=32G
#SBATCH --gpus=0
#SBATC{H} --array=0-1
#SBATCH --job-name=hep2pil
#SBATCH --output=/sdf/home/y/youngsam/data/atlas/MadGraphRuns/logs/p2r-%j.log
#SBATCH --error=/sdf/home/y/youngsam/data/atlas/MadGraphRuns/logs/p2r-%j.err
#SBATCH --time=0-10:00:00 
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=youngsam@stanford.edu

######## parameters ########
# RUN_ID=$SLURM_ARRAY_TASK_ID
DELPHES_DIR=/sdf/home/y/youngsam/data/atlas/Delphes
OUTPUT_DIR=/sdf/home/y/youngsam/data/atlas/pileup_generation/hepmc
INPUT_DIR=/sdf/home/y/youngsam/data/atlas/pileup_generation/hepmc/batch

START_IDX=1200
END_IDX=1799
############################

file_list=""

cd $INPUT_DIR
for ((i=$START_IDX; i<$END_IDX; i++)); do
    z_filled_idx=$(printf "%04d" $i)
    file_list+="10000e_${z_filled_idx}.hepmc "
done
$DELPHES_DIR/hepmc2pileup "${OUTPUT_DIR}/pileup/MinBias_6m_${START_IDX}-${END_IDX}.pileup" $file_list