## QCD background simulation

The QCD background is simulated using MadGraph. Because we're using SM particles, we can use the default MadGraph installation. However you will need to copy over the `delphes_card_NNVX.tcl` and `delphes_card_NNVX_pileup.tcl` files to your Delphes installation, and make sure they point to whatevever pileup file you're using correctly. This file modifies the default NNVX card to include particle information in the ROOT output.

The process is the following:

1. Generate the QCD background using MadGraph (outputs the directory QCD_multijet)
2. Run the simulation using Pythia (outputs the HEPMC3 file)
3. Run the detector simulation using Delphes (outputs the ROOT file)

This is fully automated using the `QCD_multijet.sh` SLURM script. Make sure to modify the script to point to the correct MadGraph and Delphes installations, and to change the number of events to generate. Run the batch script using

```bash
sbatch QCD_multijet.sh
```

You can also just run the script locally bia `bash QCD_multijet.sh` if you don't have access to a SLURM cluster.

## LLP signal simulation


The LLP signal is generated using MadGraph. Because we're using BSM particles, you will need to copy the files in the `MadGraph` directory to your MadGraph installation. Also, ensure that the `delphes_card_NNVX.tcl` and `delphes_card_NNVX_pileup.tcl` files are in your Delphes installation's `cards` directory, and that they points to to whatevever pileup file you're using correctly.

The process for generating a single mass sample is the following:

1. Generate the LLP signal using MadGraph (outputs the directory `neutralLLP_MS_1_4`)
2. Decay the signal using Madspin (outputs the directory `decay_1000025_0`)
3. Shower the decayed signal using Pythia (outputs the HEPMC3 file)
4. Run the detector simulation using Delphes (outputs the ROOT file)

This is fully automated using the `NNVX.sh` SLURM script. Parameters such as the mass of the LLP and the number of events to generate can be modified in the script. Run the batch script using

```bash
sbatch NNVX.sh
```

You can also just run the script locally bia `bash NNVX.sh` if you don't have access to a SLURM cluster.


## Dark shower simulation

The dark shower is generated using Pythia using a custom `.cmnd` file. The process is the following:

1. Compile `cmnd2hepmc` by running `make cmnd2hepmc` in the `Delphes` directory. Make sure you set the correct paths to Pythia and HepMC in `Makefile.inc`.
2. Generate the dark shower with Pythia using `Delphes/cmnd2hepmc` (outputs the HEPMC file)
2. Run the detector simulation using Delphes (outputs the ROOT file)

This is fully automated (except for the compiling part) using the `darkshower.sh` SLURM script. The `cmnd` file, `cards/cmnds/Nc3Nf3_sFoff_pp_2pi_decay_lam_10_probvec_75.cmnd`, was taken from the [`antelope-datasets`](https://gitlab.cern.ch/jgonski/antelope-datasets/-/tree/master) repo, though in principle you can run any dark shower you want. Run the batch script using

```bash
sbatch darkshower.sh
```

## Simulating pileup (genuine pileup)

The pileup is simulated using a custom `.cmnd` file. The process is the following:

1. Ensure `cmnd2hepmc` is compiled (see step 1 of the dark shower simulation)
2. Generate the pileup with Pythia using `Delphes/cmnd2hepmc` (outputs the HEPMC file)

If you want to simulate pileup and signal/background separately, you can simulate pileup through the ATLAS detector:

3. Run the detector simulation using Delphes (outputs the ROOT file)

Or, if you want Delphes to replace MinBias.pileup with a larger statistics pileup file for Delphes to automatically combine with signal/background,

3. Convert the HEPMC file to a pileup file using `Delphes/hepmc2pileup` (outputs the pileup file)
4. Replace the pileup file pointer in `delphes_card_NNVX_pileup.tcl` with the new pileup file.

For the separated pileup simulation, you can use the following scripts:

* `pileup_generation.sh` to generate a bunch of HEPMC pileup events in parallel
* `pileup_to_root.sh` to simulate each HEPMC event in the ATLAS detector and save them as ROOT files.
* Modify `to_h5.py` to add pileup to the signal/background files. (sorry, I haven't gotten this far)

For the combined pileup simulation, you can use the following scripts:
* `pileup_generation.sh` to generate a bunch of HEPMC pileup events in parallel
* `hepmc2pileup.sh` to convert HEPMC file(s) to a single .pileup file
* `<name>_pileup.sh` to simulate the signal/background with the new pileup file. Ensure that the filepath in `delphes_card_NNVX_pileup.tcl` is correct.

## Simulating pileup (synthetic)

We have looked at using Gaussian Mixture models as a way to simulate pileup. The idea is to train a GMM on the event-level track distributions of a real dataset, and then train another GMM on each fitted event-level GMM; to simulate a new pileup event, we sample a new track distribution (modeled as a sum of Gaussians) from the higher level distribution, which we then sample from to find each track. A coarse graphic showing this process is shown below:

![GMM pileup](img/HGMM.png)

This model is implemented in `models/hgmm.py`, and was trained on the 1,000 event pileup dataset provided by Delphes (MinBias.pileup). See `notebooks/hgmm.ipynb` for a demonstration of the model. The following process is used to generate synthetic pileup:

1. Train the GMM on the event-level track distributions of the real dataset (done already with a small dataset, saved as `models/hgmm.pkl`). See `hgmm.ipynb` to see how to train it.
2. Run `simulate_gmm_pileup.py` (read through the script before running and modify as needed)
3. Run `to_h5_with_gmm_pileup.py` (read through the script before running and modify as needed)