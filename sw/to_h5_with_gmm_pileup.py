#!/usr/bin/env python

"""
Author: Sam Young (youngsam@stanford.edu)

Converts ROOT signal/background files to h5 files using synthetic pileup generated with a HGMM and saved as parquet files.
"""

import uproot
import h5py
import numpy as np
from tqdm import tqdm
import awkward as ak
import os

def get_pileup_events(start, stop):
    X = []
    for i in tqdm(range(start, stop), desc="Loading pileup events", position=0):
        X.append(
            ak.from_parquet(
                f"/sdf/home/y/youngsam/data/atlas/pileup_generation/parquet/pileup_{i}.parquet"
            )
        )
    return ak.Array(
        [{p: X[i][p].to_list() for p in X[0].fields} for i in range(len(X))]
    )

# Load data from ROOT file
# Assume `file.root` is your ROOT file and `tree` is your TTree name
def root_to_h5(root_file, output_dir, batch_size=10000, prune_size=None, pt_cut=0.5, pileup_start=0):
    output_file = os.path.join(output_dir, os.path.basename(root_file).replace('.root', '_pileup.h5'))
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    if os.path.exists(output_file):
        print(f"File {output_file} already exists, skipping")
        return output_file
    
    h5_file = h5py.File(output_file, "w")

    file = uproot.open(root_file)
    tree = file["Delphes"]
    
    print(root_file.replace('.root', '.h5'))

    branches = ['Track.PT', 'Track.Eta', 'Track.Phi', 'Track.D0', 'Track.DZ']

    # create a dataset
    dtype = np.dtype(
        [
            (br, np.float32) for br in branches
        ]
    )

    dset = h5_file.create_dataset('Track', (0,), maxshape=(None,), dtype=dtype, chunks=True)
    dset_index = h5_file.create_dataset('lengths', (0,), maxshape=(None,), dtype=np.int32, chunks=True)
    
    if prune_size is not None:
        prune_slice = slice(None, prune_size)
    else:
        prune_slice = slice(None)


    indices = []
    total_events = tree.num_entries

    for start in tqdm(range(0, total_events, batch_size), position=1):
        stop = start + batch_size
        out = tree.arrays(branches, entry_start=start, entry_stop=stop, library="ak")
        pileup = get_pileup_events(
            start + pileup_start, start + len(out) + pileup_start
        )
        print(
            "added",
            len(pileup),
            f"pileup events (avg. {np.mean(ak.num(pileup['Track.PT'], axis=1)):.1f} tracks) to",
            len(out),
            f"original events (avg. {np.mean(ak.num(out['Track.PT'], axis=1)):.1f})",
        )
        out = ak.concatenate([out, pileup], axis=1)

        pt_argsort = ak.argsort(out['Track.PT'], axis=1, ascending=False)
        pt = out['Track.PT'][pt_argsort]
        pt_mask = pt > pt_cut
        data = np.zeros(len(ak.ravel(out[branches[0]][pt_mask][:,prune_slice])), dtype=dtype)

        for br in branches:
            d = out[br][pt_argsort]
            d = d[pt_mask]
            d = d[:, prune_slice]
            if br == 'Track.PT':
                d = d / ak.sum(d, axis=1, keepdims=True)            

            data[br] = ak.to_numpy(ak.ravel(d))

        lengths = ak.num(out[branches[0]][:, prune_slice], axis=1)
        indices.extend(lengths)
        dset_index[-len(lengths):] = lengths
        dset.resize(dset.shape[0] + len(data), axis=0)
        dset[-len(data):] = data

    dset_index.resize(len(indices), axis=0)
    dset_index[:] = indices

    h5_file.close()
    
    num_events = len(indices)
    
    return num_events

def main():
    import argparse

    parser = argparse.ArgumentParser(description='Convert ROOT to h5')
    # Root files to convert
    parser.add_argument('root_files', type=str, nargs='+', help='Root files to convert')
    
    # output destination
    parser.add_argument('-O', '--output', type=str, help='Output destination', required=False)

    # Batch size
    parser.add_argument('--batch-size', type=int, default=10000, help='Batch size', required=False)
    # Prune size
    parser.add_argument('--prune-size', type=int, default=None, help='Prune size', required=False)
    # p_T cut
    parser.add_argument('--pt-cut', type=float, default=0.5, help='p_T cut', required=False)
    # pileup start
    parser.add_argument('--pileup-start', type=int, default=0, help='Pileup start', required=False)

    args = parser.parse_args()
    
    pileup_start = args.pileup_start
    for root_file in args.root_files:
        pileup_start += root_to_h5(root_file, args.output, args.batch_size, args.prune_size, args.pt_cut, pileup_start)

if __name__ == '__main__':
    main()