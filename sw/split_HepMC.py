#!/usr/bin/env python

import pyhepmc as hep
from pyhepmc.io import ReaderAsciiHepMC2, WriterAsciiHepMC2
import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

def split_hepmc(input_filename, output_filename, events_per_file):
    input_file = ReaderAsciiHepMC2(input_filename)
    file_index = 0
    event_count = 0
    total_events_processed = 0

    while not input_file.failed():
        if event_count % events_per_file == 0:
            if 'output_file' in locals():
                output_file.close()
                del output_file
                logging.info(f"Closed file {output_filename+f'_{file_index}.hepmc'} after writing {event_count} events.")
            output_file = WriterAsciiHepMC2(output_filename+f"_{file_index}.hepmc")
            logging.info(f"Created new file: {output_filename+f'_{file_index}.hepmc'}")
            file_index += 1
            event_count = 0  # Reset event count for the new file

        event = hep.GenEvent()
        if input_file.read_event(event):
            output_file.write_event(event)
            event_count += 1
            total_events_processed += 1
            
            if event_count % 1000 == 0:
                logging.info(f"Processed {event_count} events")
            
        else:
            break

    if 'output_file' in locals():
        output_file.close()
        del output_file
        logging.info(f"Closed file {output_filename} after writing {event_count} events.")
    input_file.close()

    # Log the total events processed and files created
    logging.info(f"Total events processed: {total_events_processed}")
    logging.info(f"Total files created: {file_index}")


def main():
    import argparse 
    
    parser = argparse.ArgumentParser(description='Split HepMC file into smaller files')
    parser.add_argument('-I', '--input_file', type=str, required=True, help='Input HepMC file')
    parser.add_argument('-O', '--output_file', type=str, required=True, help='Output HepMC file')
    parser.add_argument('-N', '--events_per_file', type=int, required=True, help='Number of events per output file')
    
    args = parser.parse_args()
    
    split_hepmc(args.input_file, args.output_file, args.events_per_file)
    
if __name__ == '__main__':
    main()