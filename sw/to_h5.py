#!/usr/bin/env python

"""
Author: Sam Young (youngsam@stanford.edu)

Converts ROOT signal/background files to h5 files (without pileup, unless already included in the ROOT files)
"""

import uproot
import h5py
import numpy as np
from tqdm import tqdm
import awkward as ak

# Load data from ROOT file
# Assume `file.root` is your ROOT file and `tree` is your TTree name
def root_to_h5(root_file, batch_size=10000, prune_size=None, pt_cut=0.5):
    file = uproot.open(root_file)
    tree = file["Delphes"]
    
    h5_file = h5py.File(root_file.replace('.root', '.h5'), 'w')
    print(root_file.replace('.root', '.h5'))

    branches = ['Track.PT', 'Track.Eta', 'Track.Phi', 'Track.D0', 'Track.DZ']

    # create a dataset
    dtype = np.dtype(
        [
            (br, np.float32) for br in branches
        ]
    )

    dset = h5_file.create_dataset('Track', (0,), maxshape=(None,), dtype=dtype, chunks=True)
    dset_index = h5_file.create_dataset('lengths', (0,), maxshape=(None,), dtype=np.int32, chunks=True)
    
    if prune_size is not None:
        prune_slice = slice(None, prune_size)
    else:
        prune_slice = slice(None)


    indices = []
    for start in tqdm(range(0, tree.num_entries, batch_size)):
        stop = start + batch_size
        out = tree.arrays(branches, entry_start=start, entry_stop=stop, library="ak")

        pt_argsort = ak.argsort(out['Track.PT'], axis=1, ascending=False)
        pt = out['Track.PT'][pt_argsort]
        pt_mask = pt > pt_cut
        data = np.zeros(len(ak.ravel(out[branches[0]][pt_mask][:,prune_slice])), dtype=dtype)

        for br in branches:
            d = out[br][pt_argsort]
            if br == 'Track.PT':
                d = d / ak.sum(d, axis=1, keepdims=True)            
            d = d[pt_mask]

            data[br] = ak.to_numpy(ak.ravel(d[:, prune_slice]))

        lengths = ak.num(out[branches[0]][:, prune_slice], axis=1)
        indices.extend(lengths)
        dset_index[-len(lengths):] = lengths
        dset.resize(dset.shape[0] + len(data), axis=0)
        dset[-len(data):] = data

    dset_index.resize(len(indices), axis=0)
    dset_index[:] = indices

    h5_file.close()
    
    return root_file.replace('.root', '.h5')

def main():
    import argparse

    parser = argparse.ArgumentParser(description='Convert ROOT to h5')
    # Root files to convert
    parser.add_argument('root_files', type=str, nargs='+', help='Root files to convert')
    # Batch size
    parser.add_argument('--batch-size', type=int, default=10000, help='Batch size', required=False)
    # Prune size
    parser.add_argument('--prune-size', type=int, default=None, help='Prune size', required=False)
    # p_T cut
    parser.add_argument('--pt-cut', type=float, default=0.5, help='p_T cut', required=False)

    args = parser.parse_args()
    
    for root_file in args.root_files:
        root_to_h5(root_file, args.batch_size, args.prune_size, args.pt_cut)
        
        
if __name__ == '__main__':
    main()