#!/bin/bash

##################################################################
# Author: Sam Young (youngsam@stanford.edu)
#
# This script generates a bunch of LLP events in the ATLAS detector from end-to-end
# using MadGraph, Pythia, and Delphes.
#################################################################
 
#SBATCH --partition=roma
#SBATCH --account=atlas:compef
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --job-name=NNVX_fullrun
#SBATCH --output=/sdf/home/y/youngsam/data/atlas/NNVX-%j-fullrun.log
#SBATCH --error=/sdf/home/y/youngsam/data/atlas/NNVX-%j-fullrun.txt
#SBATCH --time=0-01:00:00 # 1 hour, change as needed

OUTPUT_DIR=/sdf/home/y/youngsam/data/atlas/neutralLLP_MS_1_4
DELPHES_DIR=/sdf/home/y/youngsam/sw/atlas/Delphes
MG5_DIR=/sdf/home/y/youngsam/sw/atlas/MG5_aMC_v3_5_3

######## parameters ########
RUN_NAME=run_01         # madgraph starts with run_01 and increments by 1 each time
OUTPUT_ROOT=100_events_n3_100GeV.root
LLP_MASS=1.000000e+02   # GeV
NUM_EVENTS=100000
time_of_flight=-1.0     # threshold (in mm) below which the invariant livetime
                        # is not written (-1 means not written)
############################

# generate p p --> n3 n3 and save the output to $OUTPUT_DIR
if [ ! -d $OUTPUT_DIR ]; then
# from proc_card_mg5.dat
$MG5_DIR/bin/mg5_aMC <<EOF
set low_mem_multicore_nlo_generation False
set include_lepton_initiated_processes False
set default_unset_couplings 99
set max_t_for_channel 99
set zerowidth_tchannel True
set nlo_mixed_expansion True
set group_subprocesses Auto
set ignore_six_quark_processes False
set loop_optimized_output True
set loop_color_flows False
set gauge unitary
set complex_mass_scheme False
set max_npoint_for_channel 0
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
import model $MG5_DIR/models/RPVMSSM_UFO
generate p p > n3 n3
output $OUTPUT_DIR
EOF
fi

# copy param_card.dat and madspin_card.jjj.dat to the directory
cp $MG5_DIR/param_card.dat $OUTPUT_DIR/Cards/param_card.dat
cp $MG5_DIR/madspin_card.jjj.dat $OUTPUT_DIR/Cards/madspin_card.dat

# change the LLP mass in param_card.dat to $LLP_MASS
sed -i "s/1000025 -1.000000e+02 # mneu3/1000025 -$LLP_MASS # mneu3/" $OUTPUT_DIR/Cards/param_card.dat
# change the number of events in run_card.dat
sed -i "s/[0-9]\+ = nevents/$NUM_EVENTS = nevents/" $OUTPUT_DIR/Cards/run_card.dat
# change time of flight threshold in run_card.dat
sed -i "s/-1.0 = time_of_flight/$time_of_flight = time_of_flight/" $OUTPUT_DIR/Cards/run_card.dat
# set multicore mode on
sed -i "s/# run_mode = 2/run_mode = 2/" $OUTPUT_DIR/Cards/mg5_configuration.txt

# run madspin decay + pythia showering
$MG5_DIR/bin/mg5_aMC <<EOF
launch $OUTPUT_DIR
madspin=ON
shower=Pythia8
0
0
quit
EOF

# run delphes on the HEPMC3 output
HEPMC_DIR=$OUTPUT_DIR/Events/${RUN_NAME}_decayed_1
gzip -d $HEPMC_DIR/tag_1_pythia8_events.hepmc.gz
$DELPHES_DIR/DelphesHepMC $DELPHES_DIR/cards/delphes_card_NNVX_pileup.tcl \
                          $HEPMC_DIR/$OUTPUT_ROOT \             
                          $HEPMC_DIR/tag_1_pythia8_events.hepmc \