#!/bin/bash
 
##################################################################
# Author: Sam Young (youngsam@stanford.edu)
#
##################################################################

#SBATCH --partition=ampere
#SBATCH --account=atlas:compef
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --job-name=QCDpileup
#SBATCH --output=/sdf/home/y/youngsam/data/atlas/MadGraphRuns/logs/QCDpileup-%j.log
#SBATCH --error=/sdf/home/y/youngsam/data/atlas/MadGraphRuns/logs/QCDpileup-%j.err
#SBATCH --time=0-12:00:00 # 12 hours, change as needed

OUTPUT_DIR=/sdf/home/y/youngsam/data/atlas/MadGraphRuns/QCD_multijet
DELPHES_DIR=/sdf/home/y/youngsam/data/atlas/Delphes
MG5_DIR=/sdf/home/y/youngsam/data/atlas/MG5_aMC_v3_5_3

######## parameters ########
NUM_EVENTS=200000                 # number of events
N_SUBRUNS=32                # set to number of CPU cores
OUTPUT_ROOT=200k_events_QCD_pileup.root
TIME_OF_FLIGHT=1.0
############################

echo "Setting up job site"
JOBSITE=/lscratch/youngsam/slurm_job_id_$SLURM_JOB_ID
rm -r $JOBSITE 2> /dev/null
mkdir -p $JOBSITE
export MG5_OUTPUT_DIR=$JOBSITE/QCD_multijet

# # generate p p --> [2-5]j and save the output to $OUTPUT
cd $MG5_DIR
./bin/mg5_aMC <<EOF
generate p p > j j
add process p p > j j j
add process p p > j j j j
add process p p > j j j j j
output $MG5_OUTPUT_DIR
quit
EOF

# echo "Copying MadGraph output to job site"
# cp -v -r /sdf/home/y/youngsam/data/atlas/MadGraphRuns/QCD_multijet $MG5_OUTPUT_DIR

# change the number of events in run.dat to 200,000
sed -i "s/[0-9]\+ = nevents/$NUM_EVENTS = nevents/" $MG5_OUTPUT_DIR/Cards/run_card.dat
# set multicore mode on
sed -i "s/# run_mode = 2/run_mode = 2/" $MG5_OUTPUT_DIR/Cards/mg5_configuration.txt
sed -i "s/-1.0 = time_of_flight/$TIME_OF_FLIGHT = time_of_flight/" $MG5_OUTPUT_DIR/Cards/run_card.dat

# run pythia showering
# cd $MG5_OUTPUT_DIR
# ./bin/madevent multi_run $N_SUBRUNS << EOF
./bin/mg5_aMC << EOF
launch $MG5_OUTPUT_DIR
madspin=OFF
shower=Pythia8
detector=OFF
done
done
quit
EOF

# run delphes on the HEPMC3 output
echo "Zipping subruns"
mkdir -p $OUTPUT_DIR
for (( i=0; i<N_SUBRUNS; i++ )); do
subdir=$MG5_OUTPUT_DIR/Events/run_01
gzip -d $subdir/tag_1_pythia8_events.hepmc.gz
cp $subdir/tag_1_pythia8_events.hepmc $OUTPUT_DIR/${OUTPUT_ROOT%.root}.hepmc
$DELPHES_DIR/DelphesHepMC $DELPHES_DIR/cards/delphes_card_NNVX.tcl $subdir/tag_1_pythia8_events.root $subdir/tag_1_pythia8_events.hepmc &
done
wait

echo "Merging subruns"
combine_ROOT.py -O $OUTPUT_DIR/$OUTPUT_ROOT -I $MG5_OUTPUT_DIR/Events/run_01_*/tag_1_pythia8_events.root

echo "Cleaning up..."
rm -r $MG5_OUTPUT_DIR