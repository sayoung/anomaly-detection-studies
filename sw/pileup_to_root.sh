#!/bin/bash
 
##################################################################
# Author: Sam Young (youngsam@stanford.edu)
#
# This script simulates a bunch of smaller pileup hepmc events
# in the ATLAS detector using Delphes, outputting
# simulated events as ROOT files.
###############################################################

#SBATCH --partition=roma
#SBATCH --account=atlas:usatlas
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --mem=8G
#SBATCH --gpus=0
#SBATCH --job-name=pileup
#SBATCH --array=0
#SBATCH --output=
#SBATCH --output=split_hepmc_%A_%a.out
#SBATCH --error=split_hepmc_%A_%a.err
#SBATCH --time=00:10:00 
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=youngsam@stanford.edu


######## parameters ########
RUN_ID=$SLURM_ARRAY_TASK_ID
EVENTS=10000

N_SUBRUNS=1
DELPHES_DIR=/sdf/home/y/youngsam/data/atlas/Delphes
FILE_DIRS=/sdf/home/y/youngsam/data/atlas/pileup_generation/hepmc/batch
TCL_FILE=$DELPHES_DIR/cards/delphes_card_NNVX.tcl
############################

echo "RUN ID: $RUN_ID"

export PYTHIA8=/sdf/home/y/youngsam/miniforge3
export PYTHIA8DATA=/sdf/home/y/youngsam/miniforge3/share/Pythia8/xmldoc
export PYTHIA8_DIR=/sdf/home/y/youngsam/miniforge3

echo "Setting up job site"
JOBSITE=/lscratch/youngsam/slurm_job_id_$SLURM_JOB_ID
rm -r $JOBSITE 2> /dev/null
mkdir -p $JOBSITE

for (( i=0; i<N_SUBRUNS; i++ )); do
    $DELPHES_DIR/DelphesHepMC $TCL_FILE $FILE_DIRS/10000e_${i}.root $FILE_DIRS/10000e_${i}.hepmc &
done
wait