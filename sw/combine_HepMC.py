#!/usr/bin/env python
"""
combine_HepMC.py
---------------------------------------------------------------------------------
Combine several HepMC2 files into a single one.

Note: don't use this for huge HepMC files. It'll take forever to read through each one
sequentially.

Note 2: Make sure your HepMC files are version 2, not version 3. You can check by 
reading the header of the file with "head <file.hepmc>". If it starts with "HepMC::Version 2", you're good to go.
If not, change the ReaderAsciiHepMC2 import to ReaderAsciiHepMC3.
---------------------------------------------------------------------------------
Usage:
python combine_HepMC.py -O <output_filename> --I <input_file1> <input_file2> ...
---------------------------------------------------------------------------------
Parameters:
- output_filename = name of the output file
- input_files = list of input ROOT files to combine
--------------------------------------------------------------------------------
"""

import pyhepmc as hep
from pyhepmc.io import ReaderAsciiHepMC2, WriterAsciiHepMC2
import logging
import glob
from tqdm import tqdm

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

def combine_hepmc(input_files, output_filename):
    output_file = WriterAsciiHepMC2(output_filename)
    total_events_processed = 0

    for input_filename in input_files:
        input_file = ReaderAsciiHepMC2(input_filename)
        logging.info(f"Processing file: {input_filename}")

        # Get the total number of events in the file
        total_events = sum(1 for _ in input_file)
        input_file.close()

        # Reopen the file for processing
        input_file = ReaderAsciiHepMC2(input_filename)

        with tqdm(total=total_events, unit='event', desc=f"Processing {input_filename}") as pbar:
            while not input_file.failed():
                event = hep.GenEvent()
                if input_file.read_event(event):
                    output_file.write_event(event)
                    total_events_processed += 1
                    pbar.update(1)
                else:
                    break

        input_file.close()

    output_file.close()
    logging.info(f"Total events processed: {total_events_processed}")
    logging.info(f"Combined file created: {output_filename}")

def main():
    import argparse

    parser = argparse.ArgumentParser(description='Combine multiple HepMC files into a single file')
    parser.add_argument('-I', '--input_files', type=str, required=True, help='Input HepMC files (wildcard supported)')
    parser.add_argument('-O', '--output_file', type=str, required=True, help='Output HepMC file')

    args = parser.parse_args()

    input_files = glob.glob(args.input_files)
    if not input_files:
        logging.error("No input files found.")
        return

    combine_hepmc(input_files, args.output_file)

if __name__ == '__main__':
    main()