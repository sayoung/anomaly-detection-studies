#!/usr/bin/env python

import argparse
TFile, TTree = None, None
import numpy as np

def lazy_root_import():
    global ROOT, TChain, TFile
    import ROOT
    from ROOT import TChain, TFile
    out = ROOT.gSystem.Load("libDelphes")
    if out == -1:
        raise ImportError("Could not load Delphes library")



def split_root_file(old_filename, new_filename, tree_name, N_subfiles):
    input_file = TFile(old_filename)
    input_tree = input_file.Get(tree_name)
    
    NEntries = input_tree.GetEntriesFast()
    
    n_infile = int(np.floor(NEntries / N_subfiles))
    n_lastfile = NEntries - n_infile * (N_subfiles - 1)
    
    file_counter = 0
    start_counter = 0
    for jf in range(N_subfiles):
        print(f"Creating subfile {jf+1}/{N_subfiles}...")
        newfile = TFile(f"{new_filename}_{file_counter}.root", "recreate")
        newtree = input_tree.CloneTree(0)
        
        if jf < (N_subfiles - 1):
            for je in range(n_infile):
                input_tree.GetEntry(start_counter + je)
                newtree.Fill()
            newfile.Write()
        else:
            for je in range(n_lastfile):
                input_tree.GetEntry(start_counter + je)
                newtree.Fill()
            newfile.Write()
        
        file_counter += 1
        start_counter += n_infile
        newfile.Close()
    
    input_file.Close()

def main():
    parser = argparse.ArgumentParser(description='Split a ROOT file into several subfiles.')
    parser.add_argument('--old_filename', required=True, help='Name of the original ROOT file (+path).')
    parser.add_argument('--new_filename', required=True, help='Base name for new ROOT files without extension.')
    parser.add_argument('--N_subfiles', type=int, required=True, help='Number of subfiles to create.')
    
    args = parser.parse_args()

    lazy_root_import()
    split_root_file(args.old_filename, args.new_filename, 'Delphes', args.N_subfiles)
    
    print('###########################################################################')
    print('# Input filename:', args.old_filename)
    print('# New filenames:', args.new_filename + '_<i>.root')
    print('# Selected tree to split:', args.tree_name)
    print('# Number of subfiles to create:', args.N_subfiles)
    print('###########################################################################')

if __name__ == "__main__":
    main()
