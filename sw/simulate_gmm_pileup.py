#!/usr/bin/env python

"""
Sam Young (youngsam@stanford.edu)

Simulates pileup data using the HGMM model in parallel and saves the output to individual parquet files.
These files are then used by to_h5_with_gmm_pileup.py to create datasets with pileup.
"""


import concurrent.futures
import pickle
import awkward as ak
import numpy as np

pickled_hgmm = "/sdf/home/y/youngsam/sw/atlas/anomaly-detection/ad/sw/hgmm.pkl"
params = ["Track.PT", "Track.Eta", "Track.Phi", "Track.DZ", "Track.D0"]

with open(pickled_hgmm, "rb") as f:
    thgmm = pickle.load(f)

def select(X):
    # PT > 0.5
    X = X[X['Track.PT'] > 0.5]
    
    # |Eta| < 5
    X = X[abs(X['Track.Eta']) < 5]
    return X


def simulate_pileup(mu=60):
    n_events = np.random.poisson(mu)
    events = thgmm.sample(n_events)
    events = select(events)
    raveled = ak.Array({p: ak.ravel(events[p]) for p in params})
    return raveled
    
def run_simulation(index):
    output = simulate_pileup(mu=60)
    ak.to_parquet(
        output,
        f"/sdf/home/y/youngsam/data/atlas/pileup_generation/parquet/pileup_{index}.parquet",
    )
    return index

# Number of simulations to run
num_simulations = 100_000
print(f"Running {num_simulations} simulations")
with concurrent.futures.ThreadPoolExecutor(max_workers=16) as executor:
    futures = [executor.submit(run_simulation, i) for i in range(num_simulations)]
    for future in concurrent.futures.as_completed(futures):
        print(f"Simulation {future.result()} completed.")