# PYTHIA configuration file.
# Generated on Sat Jun 10 08:14:52 EDT 2023 with the user supplied options:
# --with-python-config=python3-config
# --with-hepmc3
# --with-python

# Install directory prefixes.
PREFIX_BIN=/sdf/home/y/youngsam/miniforge3/bin
PREFIX_INCLUDE=/sdf/home/y/youngsam/miniforge3/include
PREFIX_LIB=/sdf/home/y/youngsam/miniforge3/lib
PREFIX_SHARE=/sdf/home/y/youngsam/miniforge3/share/Pythia8

# Compilation flags (see ./configure --help for further documentation).
CXX=/sdf/home/y/youngsam/miniforge3/bin/c++
CXX_COMMON=-O2 -std=c++11 -pedantic -W -Wall -Wshadow -fPIC -pthread
CXX_SHARED=-shared
CXX_SONAME=-Wl,-soname,
LIB_SUFFIX=.so
OBJ_COMMON=

HEPMC2_USE=true
HEPMC2_CONFIG=
HEPMC2_BIN=
HEPMC2_INCLUDE=-I/sdf/home/y/youngsam/data/atlas/MG5_aMC_v3_5_3/HEPTools/hepmc/include
HEPMC2_LIB=-L/sdf/home/y/youngsam/data/atlas/MG5_aMC_v3_5_3/HEPTools/hepmc/lib -Wl,-rpath,/sdf/home/y/youngsam/data/atlas/MG5_aMC_v3_5_3/HEPTools/hepmc/lib -lHepMC
