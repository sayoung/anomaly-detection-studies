#!/bin/bash

###############################################################
# Author: Sam Young (youngsam@stanford.edu)
#
# Combines pileup generated as parquet files through HGMM with
# ROOT files containing signal/background events. 
###############################################################


#SBATCH --partition=roma
#SBATCH --account=atlas:usatlas
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --gpus=0
#SBATCH --job-name=QCDpileup
#SBATCH --time=0-4:40:00 # 12 hours, change as needed
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=youngsam@stanford.edu

python /sdf/home/y/youngsam/sw/atlas/anomaly-detection/ad/sw/to_h5_with_gmm_pileup.py \
    /sdf/home/y/youngsam/data/atlas/qcd_llp_datasets/QCD_multijet_events_200k.root \
    -O /sdf/home/y/youngsam/data/atlas/qcd_llp_datasets/pileup_50tracks/ \
    --batch-size 1000 \
    --prune-size 50 \
    --pt-cut 0.5 \
    --pileup-start 200000