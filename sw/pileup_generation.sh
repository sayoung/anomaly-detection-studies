#!/bin/bash
 
##################################################################
# Author: Sam Young (youngsam@stanford.edu)
#
# This script generates many pileup hepmc events in parallel,
# to be used as background in the ATLAS detector.
###############################################################

#SBATCH --partition=roma
#SBATCH --account=atlas:usatlas
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=32G
#SBATCH --gpus=0
#SBATCH --job-name=pileup
#SBATCH --array=5-11
#SBATCH --output=/sdf/home/y/youngsam/data/atlas/MadGraphRuns/logs/pileup_1m-%j.log
#SBATCH --error=/sdf/home/y/youngsam/data/atlas/MadGraphRuns/logs/pileup_1m-%j.err
#SBATCH --time=0-8:00:00 
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=youngsam@stanford.edu


######## parameters ########
RUN_ID=$SLURM_ARRAY_TASK_ID
EVENTS=1000000
N_SUBRUNS=2
CMND_FILE=/sdf/home/y/youngsam/sw/atlas/anomaly-detection/ad/sw/Delphes/cards/cmnds/gen_pileup.cmnd
CMND2HEPMC=/sdf/home/y/youngsam/sw/atlas/anomaly-detection/ad/sw/Delphes/cmnd2hepmc
DELPHES_DIR=/sdf/home/y/youngsam/data/atlas/Delphes
OUTPUT_DIR=/sdf/home/y/youngsam/data/atlas/pileup_generation/hepmc
############################

echo "RUN ID: $RUN_ID"

export PYTHIA8=/sdf/home/y/youngsam/miniforge3
export PYTHIA8DATA=/sdf/home/y/youngsam/miniforge3/share/Pythia8/xmldoc
export PYTHIA8_DIR=/sdf/home/y/youngsam/miniforge3

echo "Setting up job site"
JOBSITE=/lscratch/youngsam/slurm_job_id_$SLURM_JOB_ID
rm -r $JOBSITE 2> /dev/null
mkdir -p $JOBSITE

echo "Running Pythia8 on cmnd file"
for (( i=0; i<N_SUBRUNS; i++ )); do
    $CMND2HEPMC $CMND_FILE $JOBSITE/pileup_1m_run${RUN_ID}_${i}.hepmc $EVENTS &
done
wait

# zip all the subruns into a single file and copy it to the output directory
echo "Copying over subruns to $OUTPUT_DIR"
mkdir -p $OUTPUT_DIR
for (( i=0; i<N_SUBRUNS; i++ )); do
    cp $JOBSITE/pileup_1m_run${RUN_ID}_${i}.hepmc $OUTPUT_DIR/pileup_1m_run${RUN_ID}_${i}.hepmc &
done
wait

# clean up
echo "Cleaning up"
rm -r $JOBSITE/