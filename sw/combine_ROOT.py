#!/usr/bin/env python
"""
combine_ROOT.py
---------------------------------------------------------------------------------
Combine several ROOT file trees into a single tree
---------------------------------------------------------------------------------
Usage:
python combine_ROOT.py -O <output_filename> --I <input_file1> <input_file2> ...
---------------------------------------------------------------------------------
Parameters:
- output_filename = name of the output file
- input_files = list of input ROOT files to combine
--------------------------------------------------------------------------------
"""

import argparse
ROOT, TChain, TFile = None, None, None

def lazy_root_import():
    global ROOT, TChain, TFile
    import ROOT
    from ROOT import TChain, TFile
    out = ROOT.gSystem.Load("libDelphes")
    if out == -1:
        raise ImportError("Could not load Delphes library")

def combine_root_files(output_filename, tree_name, input_files):
    chain = TChain(tree_name)
    
    # Add each specified file to the chain
    for subfile in input_files:
        chain.Add(subfile)
    
    # Create a new ROOT file to store the combined tree
    combined_file = TFile(output_filename, "RECREATE")
    combined_tree = chain.CloneTree(-1, "fast")
    
    # Write and close the file
    combined_tree.Write()
    combined_file.Close()
    
    print(f"Combined tree written to {output_filename}")

def main():
    parser = argparse.ArgumentParser(description='Combine several ROOT file trees into a single tree.')
    parser.add_argument('-I', '--input_files', nargs='+', required=True, help='List of input ROOT files to combine.')
    parser.add_argument('-O', '--output_filename', required=True, help='Name of the output ROOT file.')
    
    args = parser.parse_args()
    
    lazy_root_import()
    combine_root_files(args.output_filename, 'Delphes', args.input_files)

if __name__ == "__main__":
    main()
