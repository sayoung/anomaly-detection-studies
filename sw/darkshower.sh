#!/bin/bash

#################################################################
# Sam Young (youngsam@stanford.edu)
#
# End-to-end simulation of dark showers in the ATLAS detector,
# without pileup
###############################################################
 
#SBATCH --partition=roma
#SBATCH --account=atlas:usatlas
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --gpus=0
#SBATCH --job-name=antelope_100k
#SBATCH --output=/sdf/home/y/youngsam/data/atlas/MadGraphRuns/logs/antelope_100k-%j.log
#SBATCH --error=/sdf/home/y/youngsam/data/atlas/MadGraphRuns/logs/antelope_100k-%j.err
#SBATCH --time=0-5:00:00 
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=youngsam@stanford.edu


######## parameters ########
EVENTS=6250                 # number of events
N_SUBRUNS=16                # set to number of CPU cores

CMND_FILE=/sdf/home/y/youngsam/sw/atlas/AD/anomaly-detection-studies/sw/Delphes/cards/cmnds/Nc3Nf3_sFoff_pp_2pi_decay_lam_10_probvec_75.cmnd
CMND2HEPMC=/sdf/home/y/youngsam/sw/atlas/AD/anomaly-detection-studies/sw/Delphes/cmnd2hepmc
DELPHES_DIR=/sdf/home/y/youngsam/data/atlas/Delphes
OUTPUT_DIR=/sdf/home/y/youngsam/data/atlas/MadGraphRuns/Nc3Nf3
OUTPUT_ROOT=100k_events_Nc3Nf3.root
############################

export PYTHIA8=/sdf/home/y/youngsam/miniforge3
export PYTHIA8DATA=/sdf/home/y/youngsam/miniforge3/share/Pythia8/xmldoc
export PYTHIA8_DIR=/sdf/home/y/youngsam/miniforge3

echo "Setting up job site"
JOBSITE=/lscratch/youngsam/slurm_job_id_$SLURM_JOB_ID
rm -r $JOBSITE 2> /dev/null
mkdir -p $JOBSITE

echo "Running Pythia8 on cmnd file"
for (( i=0; i<N_SUBRUNS; i++ )); do
    $CMND2HEPMC $CMND_FILE $JOBSITE/output_${i}.hepmc $EVENTS &
done
wait

# zip all the subruns into a single file and copy it to the output directory
echo "Copying over subruns and running Delphes"
mkdir -p $OUTPUT_DIR
for (( i=0; i<N_SUBRUNS; i++ )); do
    cp $JOBSITE/output_${i}.hepmc $OUTPUT_DIR/${OUTPUT_ROOT%.root}_${i}.hepmc
    $DELPHES_DIR/DelphesHepMC $DELPHES_DIR/cards/delphes_card_NNVX.tcl $JOBSITE/output_${i}.root $JOBSITE/output_${i}.hepmc &
done
wait

# merge the subruns
echo "Merging subruns"
combine_ROOT.py -O $OUTPUT_DIR/$OUTPUT_ROOT -I $JOBSITE/output_*.root

# clean up
echo "Cleaning up"
rm -r $JOBSITE/